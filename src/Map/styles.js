import styled from "styled-components";

const Styles = styled.div`
  .wrapper {
    width: 50%;
    margin-left: 300px;
  }
  img {
    max-width: 100%;
    max-height: 100%;
    display: block;
    margin: auto auto;
  }
`;
export default Styles;
