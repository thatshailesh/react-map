//  /* global google */

import React from "react";
import { compose, withProps, lifecycle } from "recompose";
import {
  withScriptjs,
  withGoogleMap,
  GoogleMap,
  Marker
} from "react-google-maps";

const envPrefix = "REACT_APP";

const gMapKey = process.env[`${envPrefix}_GOOGLE_MAP_API`];
const baseUrl = process.env[`${envPrefix}_MAPS_BASE_URL`];
const defaultZoom = process.env[`${envPrefix}_ZOOM`] || 5;
const initialCentre = {
  lat: process.env[`${envPrefix}_LAT`] || 51.5049375,
  lng: process.env[`${envPrefix}_LNG`] || -0.0964509
};

const zoomToMarkers = map => {
  const bounds = new window.google.maps.LatLngBounds();
  map.props.children.forEach(child => {
    if (child.type === Marker) {
      bounds.extend(
        new window.google.maps.LatLng(
          child.props.position.lat,
          child.props.position.lng
        )
      );
    }
  });
  map.fitBounds(bounds);
};

export const GoogleMapWrapper = compose(
  withProps({
    googleMapURL: `${baseUrl}?key=${gMapKey}&v=3.exp&libraries=geometry,drawing,places`,
    loadingElement: <div style={{ height: `100%` }} />,
    containerElement: <div style={{ height: `85vh` }} />,
    mapElement: <div style={{ height: `100%` }} />
  }),
  lifecycle({
    componentDidMount() {
      this.setState({
        zoomToMarkers
      });
    }
  }),
  withScriptjs,
  withGoogleMap
)(props => (
  <GoogleMap
    ref={props.zoomToMarkers}
    defaultZoom={defaultZoom}
    defaultCenter={initialCentre}
  >
    {props.markers.map(marker => (
      <Marker
        key={marker.driver_id}
        position={{
          lat: marker.location.latitude,
          lng: marker.location.longitude
        }}
      />
    ))}
  </GoogleMap>
));
