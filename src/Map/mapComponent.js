import React, { Component } from "react";

import { GoogleMapWrapper } from "./googleMapsWrapper";

import Slider from "../Slider/sliderComponent";
import gif from "../Loading_icon.gif";
import { getApiData } from "../axios";
import Styles from "./styles";

export default class MapComponent extends Component {
  state = {
    apiData: {},
    driversLocations: [],
    loading: true
  };

  async componentDidMount() {
    const result = await getApiData();

    this.setState({
      apiData: result.data,
      driversLocations: result.data.drivers,
      loading: false
    });
  }

  handleChange = value => {
    const { apiData, driversLocations } = this.state;
    const driversLocationClone = [...driversLocations];
    const apiDataClone = { ...apiData };

    if (value < driversLocations.length) {
      const limitedLocations = driversLocationClone.slice(0, value);

      apiDataClone.drivers = limitedLocations;

      this.setState({ apiData: apiDataClone });
    } else {
      apiData.drivers = driversLocations;
      this.setState({ apiData });
    }
  };

  render() {
    const {
      loading,
      apiData: { drivers }
    } = this.state;

    if (!loading) {
      const sliderProps = {
        min: 1,
        max: 50,
        type: "range"
      };
      return (
        <Styles>
          <div className="wrapper">
            <Slider
              {...sliderProps}
              handleChange={this.handleChange}
              color="#FF4136"
            />
          </div>
          {drivers && drivers.length && <GoogleMapWrapper markers={drivers} />}
        </Styles>
      );
    }

    return (
      <Styles>
        <div>
          <img alt="loading..." src={gif} />
        </div>
      </Styles>
    );
  }
}
