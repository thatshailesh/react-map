import axios from "axios";

const envPrefix = "REACT_APP";

const apiBaseUrl = `${process.env[`${envPrefix}_API_BASE_URL`]}`;

const lat = process.env[`${envPrefix}_LAT`] || 51.5049375;
const lng = process.env[`${envPrefix}_LNG`] || -0.0964509;
const totalLocations = process.env[`${envPrefix}_LOCATION_COUNT`] || 20;

const apiUrl = `${apiBaseUrl}?latitude=${lat}&longitude=${lng}&count=${totalLocations}`;
export const getApiData = () =>
  axios.get(apiUrl, { headers: { "Access-Control-Allow-Origin": "*" } });
