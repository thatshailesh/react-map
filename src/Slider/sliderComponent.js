import React, { useState } from "react";
import Styles from "./styles";

const Slider = ({ handleChange, type, min, max, color }) => {
  const [value, setValue] = useState(25);

  const handleOnChange = e => {
    setValue(e.target.value);

    return handleChange(e.target.value);
  };

  return (
    <Styles color={color}>
      <input
        type={type}
        min={min}
        max={max}
        value={value}
        className="slider"
        onChange={handleOnChange}
      />
      <div className="value">{value}</div>
    </Styles>
  );
};

export default Slider;
