# React Map

> View drivers locations on maps with react and google maps API.

## Table of Contents

- [Preview](#preview)
- [Installation](#installation)
- [Dependencies](#dependencies)

## Preview

![Preview](public/preview.png "Map")

## Installation

Install [Node.js](https://nodejs.org/en/), [Git](https://git-scm.com/) and then:

```sh
git clone https://gitlab.com/thatshailesh/react-map.git
cd react-map
yarn install
yarn start
cp .env.example .env
```

`yarn start` Runs the app in the development mode.
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.
The page will reload if you make edits.
You will also see any lint errors in the console.

> Note: You have to set google map api key in `.env` file.

```js
// Replace gMapKey with your own key
// src/Map/googleMapsWrapper.js
const gMapKey = `${process.env.REACT_APP_GOOGLE_MAP_API}`;
```

##### Note: Default locations count is set to **_50_** in `.env.example`

## Dependencies

- [axios](https://github.com/axios/axios) - Promise based HTTP client for the browser and node.js
- [recompose](https://github.com/acdlite/recompose) - A React utility belt for function components and higher-order components.
- [styled-components](https://github.com/styled-components) - Styled Components are one of the new ways to use CSS in modern JavaScript. It is the meant to be a successor of CSS Modules, a way to write CSS that's scoped to a single component, and not leak to any other element in the page
- [react-google-maps](https://github.com/tomchentw/react-google-maps)- React.js Google Maps integration component
